import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

Mobile.startApplication('C:\\Users\\Catalin\\Desktop\\app-regina-maria-v480-debug.apk', true)

Mobile.tap(findTestObject('Application/RM/T162280/android.widget.ImageButton0'), 0)

Mobile.setText(findTestObject('Application/RM/T162280/android.widget.EditText0'), 'contmedic@reginamaria.ro', 0)

Mobile.setText(findTestObject('Application/RM/T162280/android.widget.EditText0 (1)'), '123456Abc', 0)

Mobile.tap(findTestObject('Application/RM/T162280/android.widget.Button0 - Intra in cont'), 0)

Mobile.tap(findTestObject('Application/RM/T162280/android.widget.Button0 - Continua'), 0)

Mobile.closeApplication()

