import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

Mobile.startApplication(GlobalVariable.AppPath, false)

Mobile.tap(findTestObject('Application/RM/login/butonpacient'), 0)

//Necesita username si pass ale unui cont ce au copil asocial
//Mobile.setText(findTestObject('Application/RM/login/Email'), GlobalVariable.userName, 0)
Mobile.setText(findTestObject('Application/RM/login/Email'), 'rodica.grindei@reginamaria.ro', 0)

//Mobile.setText(findTestObject('Application/RM/login/Parola'), GlobalVariable.password, 0)
Mobile.setText(findTestObject('Application/RM/login/Parola'), 'Cioran2019#', 0)

Mobile.tap(findTestObject('Application/RM/login/Intra in cont'), 0)

Mobile.tap(findTestObject('Application/RM/login/afterloginsplash'), 0)

Mobile.tap(findTestObject('Application/RM/000001/android.widget.ImageButton0'), 0)

Mobile.tap(findTestObject('Application/RM/000001/android.widget.Switch0 - Oprit'), 0)

Mobile.tap(findTestObject('Application/RM/000001/android.widget.Button0 - Confirma'), 0)

Mobile.tap(findTestObject('Application/RM/000001/android.widget.TextView0 - Programari'), 0)

Mobile.tap(findTestObject('Application/RM/000001/android.widget.Button0 - Detalii'), 0)

Mobile.tap(findTestObject('Application/RM/000001/android.widget.TextView0 - CHECK-IN'), 0)

Mobile.tap(findTestObject('Application/RM/000001/android.widget.Button0 - CHECK-IN ONLINE'), 0)

Mobile.tap(findTestObject('Application/RM/T162357/android.widget.Button0 - Continua'), 0)

Mobile.closeApplication()

