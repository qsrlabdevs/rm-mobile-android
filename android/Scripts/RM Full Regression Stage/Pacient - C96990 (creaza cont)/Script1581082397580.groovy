import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

Mobile.startApplication(GlobalVariable.AppPath, true)

Mobile.tap(findTestObject('Application/RM/login/butonpacient'), 0)

Mobile.tap(findTestObject('Application/RM/T162307/android.widget.TextView0 - Creeaza cont'), 0)

Mobile.setText(findTestObject('Application/RM/T162307/android.widget.EditText0'), 'test@reginamaria.ro', 0)

Mobile.setText(findTestObject('Application/RM/T162307/android.widget.EditText0 - Confirmare email'), 'test@reginamaria.ro', 
    0)

Mobile.setText(findTestObject('Application/RM/T162307/android.widget.EditText0 - Parola'), '1234.Abc', 0)

Mobile.setText(findTestObject('Application/RM/T162307/android.widget.EditText0 - Confirmare parola'), '1234.Abc', 0)

Mobile.tap(findTestObject('Application/RM/T162307/android.widget.Button0 - Urmatorul pas'), 0)

Mobile.setText(findTestObject('Application/RM/T162307/android.widget.EditText0 - Nume de familie'), 'Testulescu', 0)

Mobile.setText(findTestObject('Application/RM/T162307/android.widget.EditText0 - Prenume'), 'Testache', 0)

Mobile.setText(findTestObject('Application/RM/T162307/android.widget.EditText0 - CNP'), '1870528342585', 0)

Mobile.setText(findTestObject('Application/RM/T162306/android.widget.EditText0 - Telefon'), '0744090909', 0)

Mobile.scrollToText('Urmatorul pas', FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Application/RM/T162306/android.widget.Button0 - Urmatorul pas (1)'), 0)

Mobile.tap(findTestObject('Application/RM/T162307/android.widget.TextView0 - Selecteaza o tara'), 0)

Mobile.tap(findTestObject('Application/RM/T162307/android.widget.TextView0 -  Romania'), 0)

Mobile.tap(findTestObject('Application/RM/T162307/android.widget.TextView0 - Selecteaza un judet'), 0)

Mobile.scrollToText('Teleorman', FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Application/RM/T162307/android.widget.TextView0 - Teleorman'), 0)

Mobile.tap(findTestObject('Application/RM/T162307/android.widget.TextView0 - Selecteaza un oras'), 0)

Mobile.tap(findTestObject('Application/RM/T162307/android.widget.TextView0 - Albeni'), 0)

Mobile.tap(findTestObject('Application/RM/T162307/android.widget.EditText0 - Adresa'), 0)

Mobile.hideKeyboard()

Mobile.setText(findTestObject('Application/RM/T162307/android.widget.EditText0 (2)'), 'Str. Test Nr. 5', 0)

Mobile.tap(findTestObject('Application/RM/T162307/android.widget.Button0 - Creeaza cont'), 0)

Mobile.tap(findTestObject('Application/RM/T162307/android.widget.Button0 - Continua'), 0)

Mobile.closeApplication()

