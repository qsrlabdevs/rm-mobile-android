<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>RM_HamburgerMenu</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>d6ed95ba-307f-4730-82e6-8a2542f16fae</testSuiteGuid>
   <testCaseLink>
      <guid>79fc765b-88f5-45a4-b67c-3e0fabea68cd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - C43384 (Hamburger menu)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>824a3837-6a1a-4067-9130-09709709aba6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - C96407 (Hamburger menu)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>de29a173-86c3-4753-ae94-6bbae35660b0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - C99708 (hamburger menu)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b9e0d89e-e653-45ae-9565-8506a0b25fcb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - C117544 (Hamburger menu)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
