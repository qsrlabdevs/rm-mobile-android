<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>RM_Banca de KM</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>112c2483-00a6-4c5e-aedc-2cd9bb7e0a82</testSuiteGuid>
   <testCaseLink>
      <guid>ef3f5fc4-133d-416e-9017-b7dbb1a951d0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - C43390 (banca de kilometri)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6d2cb3dd-d3a2-40e2-a280-248b4cf8dae1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - C43391 (banca de kilometri)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ebca7819-e086-429f-a853-1d7b7feb787c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - C53180 (banca de kilometri)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f3d272ae-68c9-4859-ada2-0e756d04f4a9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - C96381 (banca de kilometri)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
