<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>RM_Locatii</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>70e8200f-3b6c-4361-b3a8-e87d6b13f54d</testSuiteGuid>
   <testCaseLink>
      <guid>aca2a585-7653-498d-b8bb-4bd9d04daa1e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - C81088 (locatii)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e30e1543-b537-4fe6-b398-71fe60aa2ce1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - C81089 (locatii)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>924e2657-e67d-41f8-ae57-738e6bc989f5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - C81090 (locatii)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
