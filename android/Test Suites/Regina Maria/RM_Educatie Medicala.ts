<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>RM_Educatie Medicala</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>ce07604c-0977-4ecf-b9d7-5358a7e561b1</testSuiteGuid>
   <testCaseLink>
      <guid>b9f6af65-f165-4a59-902c-2f805deaa12e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - C43571 (educatie medicala)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7ccbe52d-daa2-404c-9ada-d52a7ca2f519</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - C43572 (educatie medicala)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fe686c6b-3569-4b90-9bda-73b4995cb52c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - C43573 (educatie medicala)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2d686059-3285-476f-b4a9-8cfcea4d5b30</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RM Full Regression Stage/Pacient - C43574 (educatie medicala)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
